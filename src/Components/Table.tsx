import * as React from "react";

type TableProps = {
  users: IUserDetails[];
};
const Table: React.FC<TableProps> = ({ users }) => (
  <table style={{ width: "100%" }}>
    <thead>
      <tr>
        <th style={{ textAlign: "left" }}>Name</th>
        <th style={{ textAlign: "left" }}>City</th>
        <th style={{ textAlign: "left" }}>Age</th>
      </tr>
    </thead>
    <tbody>
      {users.reverse().map((user, id) => (
        <tr key={user.login.uuid}>
          <td>{`${user.name.title} ${user.name.first} ${user.name.last}`}</td>
          <td>{`${user.location.city}`}</td>
          <td>{`${user.dob.age}`}</td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default Table;
