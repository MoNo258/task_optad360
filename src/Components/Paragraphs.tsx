import * as React from "react";

const Paragraphs: React.FC = () => (
  <React.Fragment>
    <p className="p-first">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc euismod arcu
      ut eros pulvinar, et pellentesque est vehicula. Quisque augue nisi,
      interdum ac lacinia semper, consequat a est. Praesent bibendum volutpat
      condimentum. Aenean ultrices placerat quam at ultrices. Nulla pretium
      venenatis dui, sit amet dictum elit viverra ut. Nunc molestie massa id
      lobortis congue. Etiam eget sagittis velit. Nullam id purus dapibus odio
      euismod tempor. Fusce at nisl vel orci viverra lacinia vel vel libero.
      Morbi volutpat tempor tellus, aliquet rhoncus nunc egestas at. Morbi sed
      ornare lacus. Fusce volutpat tincidunt magna ac convallis. Cras ac congue
      eros. Suspendisse imperdiet ante at nibh cursus, id pretium sem euismod.
      Donec sed sem at ante faucibus bibendum a at sapien.
    </p>
    <p className="p-second">
      Integer ullamcorper mauris ac leo sollicitudin pharetra. Nunc gravida
      purus ut ex consequat, laoreet facilisis lacus commodo. Sed id tellus in
      orci congue tristique. Nam volutpat ultrices justo, sit amet euismod sem
      blandit id. Sed at ultricies odio, ut iaculis felis. Nulla placerat luctus
      turpis maximus tincidunt. Sed dui risus, fermentum in nibh elementum,
      bibendum rutrum felis. Donec consectetur erat non porta aliquet. Donec
      pellentesque quam nisi, vel hendrerit tellus dignissim quis. Integer
      viverra malesuada felis non egestas. Cras convallis tempus mattis. Cras
      consectetur commodo faucibus.
    </p>
  </React.Fragment>
);

export default Paragraphs;
