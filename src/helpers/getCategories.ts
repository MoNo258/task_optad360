const twentyArray: IUserDetails[] = [];
const thirtyArray: IUserDetails[] = [];
const fortyArray: IUserDetails[] = [];
const fiftyArray: IUserDetails[] = [];
const sixtyArray: IUserDetails[] = [];
const seventyArray: IUserDetails[] = [];

export const getCategoriesArrays = (usersArray: IUserDetails[]) => {
  usersArray.map(user => {
    const age = user.dob.age;
    switch (true) {
      case age >= 20 && age < 30:
        twentyArray.push(user);
        break;
      case age >= 30 && age < 40:
        thirtyArray.push(user);
        break;
      case age >= 40 && age < 50:
        fortyArray.push(user);
        break;
      case age >= 50 && age < 60:
        fiftyArray.push(user);
        break;
      case age >= 60 && age < 70:
        sixtyArray.push(user);
        break;
      case age >= 70 && age < 80:
        seventyArray.push(user);
        break;
      default:
        return;
    }
  });
};

export const getCategoriesLength = (resultArray: number[]) => {
  resultArray.push(
    twentyArray.length,
    thirtyArray.length,
    fortyArray.length,
    fiftyArray.length,
    sixtyArray.length,
    seventyArray.length
  );
};
