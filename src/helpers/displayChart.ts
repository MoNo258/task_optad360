import { Chart, registerables } from "chart.js";
Chart.register(...registerables);

export const displayChart = (categoriesLength: number[]) => {
  const ctx = document.getElementById("myChart") as HTMLCanvasElement;

  return new Chart(ctx, {
    type: "bar",
    data: {
      labels: ["20s", "30s", "40s", "50s", "60s", "70s"],
      datasets: [
        {
          label: "# of men per age category. Nationality: FR",
          data: categoriesLength,
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)"
          ],
          borderColor: [
            "rgba(255, 99, 132, 1)",
            "rgba(54, 162, 235, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(75, 192, 192, 1)",
            "rgba(153, 102, 255, 1)",
            "rgba(255, 159, 64, 1)"
          ],
          borderWidth: 1
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });
};
