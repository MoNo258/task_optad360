export async function getSelectedUsers() {
  const recordsNumber = 1000;
  const genderFilter = "male";
  const nationalityFilter = "fr";
  const excludeData = "registered,phone,cell";
  try {
    const response = await fetch(
      `https://randomuser.me/api/?results=${recordsNumber}&gender=${genderFilter}&nat=${nationalityFilter}&exc=${excludeData}`,
      {
        method: "GET",
        headers: {
          Accept: `application/json;odata=nometadata;`
        }
      }
    );
    if (response.status === 200) {
      return (await response.json()) as IApiData;
    } else {
      throw Error(`${response.status}: ${response.statusText}`);
    }
  } catch (error) {
    return Promise.reject(error);
  }
}
