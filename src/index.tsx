import React from "react";
import ReactDOM from "react-dom";
import "semantic-ui-css/semantic.min.css";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { getSelectedUsers } from "./Api";
import Paragraphs from "./Components/Paragraphs";
import Skeleton from "./Components/Skeleton";
import Table from "./Components/Table";
import {
  displayChart,
  getCategoriesArrays,
  getCategoriesLength
} from "./helpers";

const GlobalStyle = createGlobalStyle`
  body {
    background: #F5F6FB;
    box-sizing: border-box;
    padding: 2rem;
   }
`;

const App = () => {
  const [usersData, setUsersData] = React.useState<IUserDetails[]>([]);
  const [sortedUserData, setSortedUserData] = React.useState<IUserDetails[]>(
    []
  );
  const [oldestUserData, setOldestUserData] = React.useState<IUserDetails[]>(
    []
  );
  const categoriesLength: number[] = [];
  const [showData, setShowData] = React.useState<boolean>(false);

  React.useEffect(() => {
    getSelectedUsers().then(
      data => setUsersData(data.results),
      error => console.log(error)
    );
  }, []);
  React.useEffect(() => {
    setSortedUserData(() =>
      [...usersData].sort(
        (firstUser, secondUser) =>
          new Date(secondUser.dob.date).getTime() -
          new Date(firstUser.dob.date).getTime()
      )
    );
  }, [usersData]);
  React.useEffect(() => {
    setOldestUserData(() => sortedUserData.slice(-10));
  }, [sortedUserData]);

  const handleClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    setShowData(!showData);
    getCategoriesArrays(usersData);
    getCategoriesLength(categoriesLength);
    displayChart(categoriesLength);
  };

  return (
    <div className="body-wrapper">
      <ThemeProvider theme={{ fontFamily: "sans-serif" }}>
        <GlobalStyle />
        <div className="paragraph_top">
          <Paragraphs />
        </div>
        <div className="placeholder">
          <div
            className="chart-container"
            style={{ position: "relative", height: "40vh", width: "80vw" }}
          >
            {!showData && (
              <div className="placeholder-chart">
                <Skeleton />
              </div>
            )}
            <canvas
              id="myChart"
              style={{
                display: showData ? "block" : "none"
              }}
            ></canvas>
          </div>
          <div
            className="table-container"
            style={{ position: "relative", height: "40vh", width: "80vw" }}
          >
            {!showData && (
              <div className="placeholder-table">
                <Skeleton />
              </div>
            )}
            {showData && <Table users={oldestUserData} />}
          </div>
        </div>
        <div className="button" style={{ margin: "1rem 0", padding: "0.5rem" }}>
          <button
            onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) =>
              handleClick(event)
            }
            disabled={showData}
          >
            Show details
          </button>
        </div>
        <div className="paragraph_bottom">
          <Paragraphs />
        </div>
      </ThemeProvider>
    </div>
  );
};
ReactDOM.render(<App />, document.getElementById("root"));
