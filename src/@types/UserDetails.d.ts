interface IApiData {
  results: IUserDetails[];
  info: {
    seed: string;
    results: number;
    page: number;
    version: string;
  };
}
interface IName {
  title: string;
  first: string;
  last: string;
}
interface ILocation {
  street: string;
  city: string;
  state: string;
  postcode: string;
  coordinates: {
    latitude: string;
    longitude: string;
  };
  timezone: {
    offset: string;
    description: string;
  };
}
interface ILogin {
  uuid: string; //uuid type FIXME:
  username: string;
  password: string;
  salt: string;
  md5: string;
  sha1: string;
  sha256: string;
}
type TNationality =
  | "AU"
  | "BR"
  | "CA"
  | "CH"
  | "DE"
  | "DK"
  | "ES"
  | "FI"
  | "FR"
  | "GB"
  | "IE"
  | "IR"
  | "NO"
  | "NL"
  | "NZ"
  | "TR"
  | "US";

interface IUserDetails {
  gender: string;
  name: IName;
  location: ILocation;
  email: string;
  login: ILogin;
  dob: {
    date: string;
    age: number;
  };
  id: {
    name: string;
    value: string;
  };
  picture: {
    large: string;
    medium: string;
    thumbnail: string;
  };
  nat: TNationality;
}
